ask = input("How much short change is owed ? ")

while True:
    try:
        ask = float(ask)
        if ask <= 0:
            raise ValueError # if input from user is less or equal to zero, repeat him message of input
        else:
            ask = int(ask * 100)
            coins = 0
            while not ask == 0:
                if ask > 25:
                    ask -= 25
                    coins += 1
                elif ask > 10:
                    ask -= 10
                    coins += 1
                elif ask > 5:
                    ask -= 5
                    coins += 1
                elif ask >= 1:
                    ask -= 1
                    coins += 1
                elif ask == 0:
                    print(coins)
                    break
                else:
                    print("Something goes wrong")
                    break
            break
    except ValueError:
        ask = input("Please give us money, like real money ")
        continue