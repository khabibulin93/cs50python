"""
Programm that implement "caesar" cipher algorithm in python
"""

#TODO - create empty list, append to him cypher text, then prinit

import sys
def caesar (text:str, numbers:int):
    while True:
        try:
            # check if programm gave argument, no more two or less arguments, and if it's above or equal zero
            if len(text) >= 0 and numbers <= 26 and numbers >= 1:
                # ask user for word to encrypt
                print(text)
                ask = text
                cypher_text = []
                for i in ask:
                    # check if symbol is uppercase
                    if i.isupper():
                        # check if key not overflowing latest letter, if not do this statement
                        if (numbers + ord(i)) < ord('Z'):
                            # this print i'th symbol and added key to it, so it turn to another letter
                            cypher_text.append(chr(ord(i) + numbers))
                        # catch special statement, it's about letter Z or z, if so, do this statement 
                        elif i == 'Z':
                            # this print, in special statement, take first letter 'A' and add to it
                            # remainder of division between key and length of letter plus one
                            cypher_text.append(chr(ord('A') + (numbers - 1)))
                        # if key is overflowing latest letter, do this statement
                        else:
                            # this print, because of overflowing, start from first letter 'A'
                            # and added to it remainder of division between key and lentgh of letters 
                            cypher_text.append(chr(ord('A') + (numbers % 26)))
                    # here the same statement, but for lowercase 
                    else:
                        if (numbers + ord(i)) < ord('z'):
                            cypher_text.append(chr(ord(i) + numbers))
                        elif i == 'z':
                            cypher_text.append(chr(ord('a') + (numbers - 1)))
                        else:
                            cypher_text.append(chr(ord('a') + (numbers % 26)))
                # do new line
                print(*cypher_text, sep="")
                # get out from loop
                break
            # if not raise error to simplify output
            else:
                raise ValueError
        except ValueError:
            print('Usage of script - "python caesar.py key(some number at 1 to 26(only one key))"')
            sys.exit(1)

caesar('aaa', 26)