import crypt, sys, random, string
from datetime import datetime
import hashlib
from hmac import compare_digest as compare_hash
from itertools import chain, product

class Generate:
    def generateword(self):
        ascii = string.ascii_letters # take all letters(upper and lower)
        ascii = random.sample(ascii, 5) # get random population of length 5
        word = ''.join(ascii) # create strinf from list
        return word

    def generatehash(self):
        wordh = self.generateword()
        hash = crypt.crypt(wordh, salt=crypt.METHOD_CRYPT) # encrypt with DES-based method
        return hash
    
    # this piece of code get it from https://stackoverflow.com/a/11747419
    def bruteforce(self, charset, maxlength):
        return (''.join(candidate)
            for candidate in chain.from_iterable(product(charset, repeat=i)
            for i in range(1, maxlength + 1)))


start = datetime.now()

gen = Generate()
hash_file = gen.generatehash()
attempt = 0

for s in gen.bruteforce(string.ascii_letters, 5):
    attempt += 1
    if compare_hash(crypt.crypt(s, hash_file), hash_file):
        print('Found password - %s' % (s))
        break
    else:
        continue
end = datetime.now()
print('Tries - %s' % (attempt))
print('Work time - %s' % (end - start))