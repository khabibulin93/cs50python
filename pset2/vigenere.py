"""
Programm implement vigenere cipher algorithm 
"""

import sys
import string

lascii = list(string.ascii_lowercase)
uascii = list(string.ascii_uppercase)

ask = input("plaintext - ")
key = []
cipher = []

while True:
    try:
        if len(sys.argv) == 2 and isinstance(sys.argv[1], str):
             # ask user for word to encrypt
            if len(key) < len(ask):
                key = list(sys.argv[1])
                key = key[:] * (len(ask)//len(key)) + key[:(len(ask)%len(key))]
            elif len(key) > len(ask):
                key = list(sys.argv[1])
                key = key[:len(ask)]
            else:
                for i, k in zip(ask, key):
                    # check if symbol is uppercase
                    if i.isupper():
                        # check if key not overflowing latest letter, if not do this statement
                        if (uascii.index(k.upper()) + ord(i)) < ord('Z'):
                            # this print i'th symbol and added key to it, so it turn to another letter
                            cipher.append(chr(ord(i) + uascii.index(k.upper())))
                        # catch special statement, it's about letter Z or z, if so, do this statement 
                        elif i == 'Z':
                            # this print, in special statement, take first letter 'A' and add to it
                            # remainder of division between key and length of letter plus one
                            cipher.append(chr(ord('A') + (uascii.index(k.upper()) % 26)))
                        # if key is overflowing latest letter, do this statement
                        else:
                            # this print, because of overflowing, start from first letter 'A'
                            # and added to it remainder of division between key and lentgh of letters 
                            cipher.append(chr(ord('A') + (uascii.index(k.upper()) % 25)))
                        continue
                    # here the same statement, but for lowercase 
                    else:
                        if (lascii.index(k.lower()) + ord(i)) < ord('z'):
                            cipher.append(chr(ord(i) + lascii.index(k.lower())))
                        elif i == 'z':
                            cipher.append(chr(ord('a') + (lascii.index(k.lower()) % 26)))
                        else:
                            cipher.append(chr(ord('a') + (lascii.index(k.lower()) % 25)))
                        continue
                    # do new line
                ciph = ''.join(cipher)
                print("ciphertext - %s" % (ciph))
                # get out from loop
                break
        else:
            raise ValueError
    except ValueError:
        print("Usage: python3 vigenere.py key(some word or sequance od letters)")
        sys.exit(1)