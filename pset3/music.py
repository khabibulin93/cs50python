"""
Programm implement music, in my opinion it be Toby Fox - Snowy(Undertale OST). With use pyaudio
"""
import math        #import needed modules
import pyaudio     #sudo apt-get install python-pyaudio
import wave
import numpy as np
from scipy.io import wavfile

p = pyaudio.PyAudio()     #initialize pyaudio

#See https://en.wikipedia.org/wiki/Bit_rate#Audio

BITRATE = 44100
CHANNELS = 2

class Sound(object):
    def generate(self, BITRATE, FREQUENCY, LENGTH):
        if FREQUENCY > BITRATE:
            BITRATE = FREQUENCY+100
        
        NUMBEROFFRAMES = int(BITRATE * LENGTH)
        RESTFRAMES = NUMBEROFFRAMES % BITRATE
        WAVEDATA = ''    

        #generating wawes
        for x in range(NUMBEROFFRAMES):
            WAVEDATA = WAVEDATA+chr(int(math.sin(x/((BITRATE/FREQUENCY)/math.pi))*127+128))    

        for x in range(RESTFRAMES): 
            WAVEDATA = WAVEDATA+chr(128)
        
        return WAVEDATA

s = Sound()

stream = p.open(format = pyaudio.paInt16, 
                channels = CHANNELS, 
                rate = BITRATE, 
                output = True)

freq_list = {'A':440,'B':550,'C':660, 'D':770, 'E':880, 'D':990, 'F':1100, 'G':1210}
frames = []

for f in sorted(freq_list.values()):
    wave = s.generate(BITRATE, f, 1)
    stream.write(wave)
    frames.append(wave)
stream.stop_stream()
stream.close()
p.terminate()

framesAll = frames.encode()
#Use numpy to format data and reshape.  
#PyAudio output from stream.read() is interlaced.
result = np.fromstring(framesAll, dtype=np.int16)
chunk_length = len(result) // CHANNELS
result = np.reshape(result, (chunk_length, CHANNELS))

wavfile.write('tone.wav', BITRATE, result)