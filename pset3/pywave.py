
import math
import pyaudio
from itertools import *

p = pyaudio.PyAudio()

def sine_wave(frequency=440.0, framerate=44100, amplitude=0.5):
    '''
    Generate a sine wave at a given frequency of infinite length.
    '''
    period = int(framerate / frequency)
    if amplitude > 1.0: amplitude = 1.0
    if amplitude < 0.0: amplitude = 0.0
    lookup_table = [float(amplitude) * math.sin(2.0*math.pi*float(frequency)*(float(i%period)/float(framerate))) for i in range(period)]
    return (lookup_table[i%period] for i in count(0))

stream = p.open(format = pyaudio.paFloat32,
                channels=2,
                rate=44100,
                output=True)

# play. May repeat with different volume values (if done interactively)
stream.write(sine_wave())
stream.stop_stream()
stream.close()

p.terminate()