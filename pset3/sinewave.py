import struct
import numpy as np
import pyaudio
from scipy import signal as sg

p = pyaudio.PyAudio()

Fs = 44100                    ## Sampling Rate
f = 440                       ## Frequency (in Hz)
sample = 44100                ## Number of samples 
x = np.arange(sample)

####### sine wave ###########
y = 100*np.sin(2 * np.pi * f * x / Fs)

####### square wave ##########
#y = 100* sg.square(2 *np.pi * f *x / Fs )

####### square wave with Duty Cycle ##########
#y = 100* sg.square(2 *np.pi * f *x / Fs , duty = 0.8)

####### Sawtooth wave ########
#y = 100* sg.sawtooth(2 *np.pi * f *x / Fs )

## Open as Signed 8-bit on Audacity - Watch Video for instructions

stream = p.open(format = p.get_format_from_width(1), 
                channels = 2, 
                rate = Fs, 
                output = True)
for d in y[:]:
    stream.write(d)
stream.stop_stream()
stream.close()
p.terminate()