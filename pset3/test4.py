import pyaudio
import numpy as np
from scipy.io import wavfile

p = pyaudio.PyAudio()

volume = 1.0     # range [0.0, 1.0]
fs = 44100       # sampling rate, Hz, must be integer
duration = 1.0   # in seconds, may be float
freq = {'A':440, 'A#':466.16,'Bb':466.16, 'B':493.88, 'Cb':493.88, 'B#':523.25, 'C':523.25, 'C#': 554.37 
,'Db':554.37, 'D':587.33,'D#':622.25,'Eb':622.25, 'E':659.26, 'Fb':659.26,'E#':698.46, 'F':698.46
,'F#':739.99,'Gb':739.99, 'G':783.99, 'G#': 830.61, 'Ab':830.61} # sine frequency, Hz, may be float
# generate samples, note conversion to float32 array
samples = []
for f in sorted(freq.values()):
    samples.append((np.sin(2*np.pi*np.arange(fs*duration)*f/fs)).astype(np.float32))

# for paFloat32 sample values must be in range [-1.0, 1.0]
stream = p.open(format = pyaudio.paFloat32,
                channels=2,
                rate=fs,
                output=True)

# play. May repeat with different volume values (if done interactively)
"""for sample in samples:
    stream.write(sample)
stream.stop_stream()
stream.close()

p.terminate()"""

framesAll = b''.join(samples)

#Use numpy to format data and reshape.  
#PyAudio output from stream.read() is interlaced.
result = np.fromstring(framesAll, dtype=np.float32)
chunk_length = len(result) // 2
result = np.reshape(result, (chunk_length, 2))

#Write multi-channel .wav file with SciPy
wavfile.write("palure.wav", fs, result)